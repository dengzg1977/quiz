package domain.admin.impl;

import com.owl.quiz.domain.admin.NewQuestionDTO;
import com.owl.quiz.domain.admin.QuestionAdminService;
import com.owl.quiz.domain.admin.impl.QuestionAdminServiceImpl;
import com.owl.quiz.domain.admin.AdminQuestionInBankDAO;
import com.owl.quiz.domain.entity.QuestionInBank;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class QuestionAdminServiceImplTest {
    @InjectMocks
    QuestionAdminService questionAdminService=new QuestionAdminServiceImpl();
    @Mock
    AdminQuestionInBankDAO bankDAO;
    @Test
    public void newQuestion() {
        NewQuestionDTO newQuestionForm= NewQuestionDTO.builder()
                .answer(3)
                .options(new String[]{"option1","option2","option3","option3"})
                .question("question description")
                .tags(new String[]{"tag1","tag2"})
                .build();
        QuestionInBank questionInBank = questionAdminService.newQuestion(newQuestionForm);
        assertEquals(questionInBank.getRightAnswer(),newQuestionForm.getAnswer());
        assertEquals(questionInBank.getQuestion(),newQuestionForm.getQuestion());
        assertArrayEquals(questionInBank.getTag(),newQuestionForm.getTags());
        assertArrayEquals(questionInBank.getOptions(),newQuestionForm.getOptions());
    }
}