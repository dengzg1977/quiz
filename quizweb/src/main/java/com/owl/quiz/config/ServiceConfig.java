package com.owl.quiz.config;

import com.owl.quiz.domain.admin.QuestionAdminService;
import com.owl.quiz.domain.admin.impl.QuestionAdminServiceImpl;
import com.owl.quiz.domain.admin.AdminQuestionInBankDAO;
import com.owl.quiz.domain.quiz.QuizDAO;
import com.owl.quiz.domain.quiz.QuizService;
import com.owl.quiz.domain.quiz.impl.QuizServiceImpl;
import lombok.extern.java.Log;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Log
public class ServiceConfig {

    @Bean
    QuizService quizService(QuizDAO quizDAO){
        QuizServiceImpl quizService=new QuizServiceImpl();
        quizService.setQuizDAO(quizDAO);
        return quizService;
    }
    @Bean
    QuestionAdminService questionAdminService(AdminQuestionInBankDAO bankDAO){
        log.info("init questionAdminService");
        QuestionAdminServiceImpl questionAdminService=new QuestionAdminServiceImpl();
        questionAdminService.setBankDAO(bankDAO);
        return questionAdminService;
    }
}
