package com.owl.quiz.domain.admin;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class NewQuestionDTO {
    private String question;
    private String[] options;
    private String[] tags;
    private int answer;
}
