package com.owl.quiz.controller;

import com.owl.quiz.controller.dto.*;
import com.owl.quiz.domain.quiz.AnswerDTO;
import com.owl.quiz.domain.quiz.QuestionDTO;
import com.owl.quiz.domain.quiz.QuizResult;
import com.owl.quiz.domain.quiz.QuizService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class QuizController {
    public static final int QUIZ_SIZE = 30;
    @Autowired
    QuizService quizService;

    @PostMapping("/quiz/create")
    public BaseResult<String> newQuiz(){
        return BaseResult.getResult(quizService.createNewQuiz(QUIZ_SIZE).getId());
    }
    @PostMapping("/quiz/post_answer")
    public BaseResult<AnswerDTO> postAnswer(@RequestBody AnswerForm answerForm){
        return BaseResult.getResult(quizService.submitAnswer(answerForm.getId(), answerForm.getAnswer()));
    }
    @GetMapping("/quiz/{quizId}/get_question")
    public BaseResult<QuestionDTO> getQuestion(@PathVariable("quizId")String quizId){
        return BaseResult.getResult(quizService.getQuizCurrentQuestion(quizId));
    }
    @GetMapping("/quiz/{quizId}/show_result")
    public BaseResult<QuizResult> getQuizResult(@PathVariable("quizId")String quizId){
        return BaseResult.getResult(quizService.getQuizResult(quizId));
    }
}
