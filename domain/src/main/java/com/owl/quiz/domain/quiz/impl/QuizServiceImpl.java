package com.owl.quiz.domain.quiz.impl;

import com.owl.quiz.domain.entity.Question;
import com.owl.quiz.domain.entity.QuestionInBank;
import com.owl.quiz.domain.entity.Quiz;
import com.owl.quiz.domain.quiz.QuizDAO;
import com.owl.quiz.domain.quiz.*;
import lombok.Data;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class QuizServiceImpl implements QuizService {
    private QuizDAO quizDAO;

    public void setQuizDAO(QuizDAO quizDAO) {
        this.quizDAO = quizDAO;
    }

    @Override
    public Quiz createNewQuiz(int size) {
        List<String> allQuestionsInBank = quizDAO.getAllQuestionsIdInBank();
        Quiz quiz = newQuizFromBank(size, allQuestionsInBank);
        quizDAO.saveQuiz(quiz);
        return quiz;
    }

    private Quiz newQuizFromBank(int size, List<String> allQuestionsInBank) {
        Collections.shuffle(allQuestionsInBank);
        return Quiz.of(allQuestionsInBank.stream()
                .map(Question::init)
                .limit(size)
                .collect(Collectors.toList()));
    }

    @Override
    public QuestionDTO getQuizCurrentQuestion(String quizId) {
        Question question = quizDAO.getQuizById(quizId).getCurrentQuestion();
        QuestionInBank questionInBank = quizDAO.findQuestionInBankById(question.getQuestionId());
        return getQuestion(questionInBank);
    }

    private QuestionDTO getQuestion(QuestionInBank questionInBank) {
        return QuestionDTO.builder()
                .question(questionInBank.getQuestion())
                .options(questionInBank.getOptions())
                .build();
    }

    @Override
    public AnswerDTO submitAnswer(String quizId, int answer) {
        Quiz quiz = quizDAO.getQuizById(quizId);
        submitAnswer(answer, quiz);
        if (quiz.isOver()) {
            quizDAO.updateRank(quiz);
        }
        return getAnswerResult(quiz);
    }

    private AnswerDTO getAnswerResult(Quiz quiz) {
        return AnswerDTO.builder()
                .hasNext(quiz.hasNext())
                .score(quiz.getScore())
                .build();
    }

    private void submitAnswer(int answer, Quiz quiz) {
        quiz.submitAnswerAndGoNext(answer,quizDAO.findQuestionInBankById(quiz.getCurrentQuestion().getQuestionId()));
        quizDAO.saveQuiz(quiz);
    }

    @Override
    public QuizResult getQuizResult(String quizId) {
        Quiz quiz = quizDAO.getQuizById(quizId);
        return getQuizResult(quiz);
    }

    private QuizResult getQuizResult(Quiz quiz) {
        return QuizResult.builder()
                .score(quiz.getScore())
                .id(quiz.getId())
                .rank(quizDAO.getRank(quiz))
                .build();
    }

}
