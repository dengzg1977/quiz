package domain.quiz.impl;

import com.owl.quiz.domain.entity.Question;
import com.owl.quiz.domain.entity.QuestionInBank;
import com.owl.quiz.domain.entity.Quiz;
import com.owl.quiz.domain.quiz.QuizDAO;
import com.owl.quiz.domain.quiz.*;
import com.owl.quiz.domain.quiz.impl.QuizServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class QuizServiceImplTest {
    public static final String QUIZID = "quizid";
    public static final int RIGHT_ANSWER = 2;
    public static final String QUESTION_DESCRIPTION = "question description";
    public static final String QUESTIONID = "questionid";
    public static final int RANK = 100;
    @InjectMocks
    QuizService quizService=new QuizServiceImpl();
    @Mock
    QuizDAO quizDAO;
    @Test
    public void getRandomQuiz() {
        List<String> ids = IntStream.range(0, 100)
                .mapToObj(i -> String.valueOf(i))
                .collect(Collectors.toList());
        when(quizDAO.getAllQuestionsIdInBank()).thenReturn(ids);
        Quiz randomQuiz = quizService.createNewQuiz(30);
        assertEquals(randomQuiz.getProgress(),0);
        assertEquals(randomQuiz.getQuestions().size(),30);
    }

    @Test
    public void getCurrentQuestion() {
        QuestionInBank questionInBank = QuestionInBank.builder()
                .rightAnswer(RIGHT_ANSWER)
                .options(new String[]{"1", "2", "3", "4"})
                .rightAnswer(2)
                .question(QUESTION_DESCRIPTION)
                .build();
        Question question=createQuestion(questionInBank);
        Quiz quiz=Quiz.builder()
                .progress(1)
                .questions(Arrays.asList(
                        Question.builder().rightAnswer(true).build(),
                        question
                ))
                .build();

        when(quizDAO.getQuizById(QUIZID)).thenReturn(quiz);
        when(quizDAO.findQuestionInBankById(question.getQuestionId())).thenReturn(questionInBank);

        assertEquals(quizService.getQuizCurrentQuestion(QUIZID), QuestionDTO.builder()
                .options(questionInBank.getOptions())
                .question(questionInBank.getQuestion())
                .build());
    }

    @Test
    public void updateAnswer() {
        QuestionInBank questionInBank = QuestionInBank.builder()
                .rightAnswer(RIGHT_ANSWER)
                .options(new String[]{"1", "2", "3", "4"})
                .rightAnswer(2)
                .question(QUESTION_DESCRIPTION)
                .build();
        Quiz quiz=Quiz.builder()
                .progress(0)
                .questions(Arrays.asList(createQuestion(questionInBank),createQuestion(questionInBank),createQuestion(questionInBank),createQuestion(questionInBank)))
                .build();
        when(quizDAO.getQuizById(QUIZID)).thenReturn(quiz);
        when(quizDAO.findQuestionInBankById(QUESTIONID)).thenReturn(questionInBank);


        for(int i=0;i<3;i++){
            AnswerDTO answerResult=quizService.submitAnswer(QUIZID,2);
            assertTrue(answerResult.isHasNext());
            assertEquals(answerResult.getScore(),i+1);
            verify(quizDAO,times(0)).updateRank(anyObject());
        }
        AnswerDTO answerResult=quizService.submitAnswer(QUIZID,2);
        assertFalse(answerResult.isHasNext());
        assertEquals(answerResult.getScore(),4);
        verify(quizDAO,times(1)).updateRank(quiz);
    }

    private Question createQuestion(QuestionInBank questionInBank){
        return Question.builder()
                .rightAnswer(false)
                .questionId(QUESTIONID)
                .answer(-1)
                .build();
    }

    @Test
    public void testGetResult(){
        Quiz quiz=Quiz.builder()
                .questions(Arrays.asList(
                        Question.builder().rightAnswer(true).build(),
                        Question.builder().rightAnswer(true).build(),
                        Question.builder().rightAnswer(true).build(),
                        Question.builder().rightAnswer(false).build()
                ))
                .build();
        when(quizDAO.getQuizById(QUIZID)).thenReturn(quiz);
        when(quizDAO.getRank(quiz)).thenReturn(RANK);

        QuizResult quizResult = quizService.getQuizResult(QUIZID);
        assertEquals(quizResult.getRank(),RANK);
        assertEquals(quizResult.getScore(),3);

    }
}