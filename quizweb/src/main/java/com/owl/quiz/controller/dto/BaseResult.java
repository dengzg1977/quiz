package com.owl.quiz.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BaseResult<T> {
    private T data;
    public static BaseResult getResult(Object data){
        return BaseResult.builder().data(data).build();
    }
}
