package com.owl.quiz.controller;

import com.owl.quiz.domain.admin.NewQuestionDTO;
import com.owl.quiz.domain.admin.QuestionAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AdminController {
    @Autowired
    QuestionAdminService adminService;
    @PostMapping("/admin/new_question")
    public String newQuestion(@RequestBody NewQuestionDTO newQuestionForm){
        adminService.newQuestion(newQuestionForm);
        return "";
    }
}
