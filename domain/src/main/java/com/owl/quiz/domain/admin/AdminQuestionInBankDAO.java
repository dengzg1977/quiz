package com.owl.quiz.domain.admin;

import com.owl.quiz.domain.entity.QuestionInBank;

public interface AdminQuestionInBankDAO {

    QuestionInBank creteOrUpdate(QuestionInBank question);
}
