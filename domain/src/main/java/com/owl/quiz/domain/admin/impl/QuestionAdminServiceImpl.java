package com.owl.quiz.domain.admin.impl;

import com.owl.quiz.domain.admin.NewQuestionDTO;
import com.owl.quiz.domain.admin.QuestionAdminService;
import com.owl.quiz.domain.admin.AdminQuestionInBankDAO;
import com.owl.quiz.domain.entity.QuestionInBank;
import lombok.Data;

import java.util.UUID;

@Data
public class QuestionAdminServiceImpl implements QuestionAdminService {
    private AdminQuestionInBankDAO bankDAO;
    @Override
    public QuestionInBank newQuestion(NewQuestionDTO newQuestionForm) {
        QuestionInBank questionInBank = QuestionInBank.builder()
                .id(UUID.randomUUID().toString())
                .question(newQuestionForm.getQuestion())
                .rightAnswer(newQuestionForm.getAnswer())
                .options(newQuestionForm.getOptions())
                .tag(newQuestionForm.getTags())
                .build();
        bankDAO.creteOrUpdate(questionInBank);
        return questionInBank;
    }
}
