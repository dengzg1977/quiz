package com.owl.quiz.controller.dto;

import lombok.Data;

@Data
public class AnswerForm {
    private String id;
    private int answer;
}
