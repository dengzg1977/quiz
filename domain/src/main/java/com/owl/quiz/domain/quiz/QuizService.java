package com.owl.quiz.domain.quiz;


import com.owl.quiz.domain.entity.Quiz;

public interface QuizService {

    Quiz createNewQuiz(int questionCount);

    QuestionDTO getQuizCurrentQuestion(String quizId);

    AnswerDTO submitAnswer(String quizId, int answer);

    QuizResult getQuizResult(String quizId);
}
