package com.owl.quiz.domain.admin;

import com.owl.quiz.domain.entity.QuestionInBank;

public interface QuestionAdminService {
    QuestionInBank newQuestion(NewQuestionDTO newQuestionForm);
}
