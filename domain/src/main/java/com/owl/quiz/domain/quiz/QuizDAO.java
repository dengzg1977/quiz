package com.owl.quiz.domain.quiz;

import com.owl.quiz.domain.entity.QuestionInBank;
import com.owl.quiz.domain.entity.Quiz;

import java.util.List;


public interface QuizDAO {

    List<String> getAllQuestionsIdInBank();

    void saveQuiz(Quiz quiz);

    Quiz getQuizById(String id);

    void updateRank(Quiz quiz);

    int getRank(Quiz quiz);

    QuestionInBank findQuestionInBankById(String questionId);
}
