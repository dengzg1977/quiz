package com.owl.quiz.dao;

import com.owl.quiz.domain.admin.AdminQuestionInBankDAO;
import com.owl.quiz.domain.quiz.QuizDAO;
import com.owl.quiz.domain.entity.QuestionInBank;
import com.owl.quiz.domain.entity.Quiz;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class MemoryQuizDAOImpl implements QuizDAO,AdminQuestionInBankDAO {
    static Map<String,Quiz> quizMap=new HashMap<>();
    static Map<Integer,Integer> rankMap=new HashMap<>();
    static Map<String,QuestionInBank> questionInBanks=new HashMap<>();
    @PostConstruct
    public void init(){
        questionInBanks.put("1", QuestionInBank.builder()
                .options(new String[]{"2","3","4","5"})
                .rightAnswer(0)
                .question("1+1=?")
                .id("1")
                .tag(new String[]{"math"})
                .build());
        questionInBanks.put("2", QuestionInBank.builder()
                .options(new String[]{"2","3","4","5"})
                .rightAnswer(1)
                .question("1+2=?")
                .id("2")
                .tag(new String[]{"math"})
                .build());
        questionInBanks.put("3", QuestionInBank.builder()
                .options(new String[]{"2","3","4","5"})
                .rightAnswer(2)
                .question("1+3=?")
                .id("3")
                .tag(new String[]{"math"})
                .build());
        questionInBanks.put("4", QuestionInBank.builder()
                .options(new String[]{"2","3","4","5"})
                .rightAnswer(3)
                .id("4")
                .question("1+4=?")
                .tag(new String[]{"math"})
                .build());
    }

    @Override
    public QuestionInBank creteOrUpdate(QuestionInBank question) {
        return questionInBanks.put(question.getId(),question);
    }
    @Override
    public void saveQuiz(Quiz quiz) {
        quizMap.put(quiz.getId(),quiz);
    }

    @Override
    public Quiz getQuizById(String id) {
        Quiz quiz = quizMap.get(id);
        return quiz;
    }
    @Override
    public void updateRank(Quiz quiz) {
        int score=quiz.getScore();
        Integer count = rankMap.getOrDefault(score, 0);
        rankMap.put(score,count+1);
    }

    @Override
    public int getRank(Quiz quiz) {
        int score=quiz.getScore();
        int total=rankMap.entrySet().stream()
                .mapToInt(entry->entry.getValue())
                .sum();
        int top=rankMap.entrySet().stream()
                .filter(entry->entry.getKey()>=score)
                .mapToInt(entry->entry.getValue())
                .sum();
        return top*100/total;
    }

    @Override
    public List<String> getAllQuestionsIdInBank() {
        return questionInBanks.values()
                .stream()
                .map(question -> question.getId())
                .collect(Collectors.toList());
    }

    @Override
    public QuestionInBank findQuestionInBankById(String questionId) {
        return questionInBanks.get(questionId);
    }
}
